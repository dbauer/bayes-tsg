def memoize(fun):
    cache = {}
    def new_fun(*args):
        inputs = tuple(args)
        try: 
            return cache[inputs]
        except KeyError: 
            result = fun(*args)
            cache[inputs] = result
            return result
    return new_fun


