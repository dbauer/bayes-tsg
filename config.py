# Parameter for the geometric distribution to use
p = 0.5

# Dmoothing parameter for pcfg laplace smoothing.
# This smoothing is somewhat dumb. We just add 1 and assign the 
# remaining probability mass to the bucket of unseen rules for 
# each nonterminal. 
pcfg_alpha = 1


# Concentration parameter for the Dirichlet Process
alpha = 1 

iterations = 500

# parser beam
beam = 8  
