from grammar.psg import StringGrammar, StringRule
from grammar.tsg import TreeSubstitutionGrammar
from grammar.cfg import CFG
from grammar.fancy_tree import FancyTree, Treebank
from collections import defaultdict
from itertools import product
import config
import math
import sys
import unittest
import pprint
import heapq as hq

def preprocess_sent(sentence):
    try: 
        sentence = sentence.split()
    except: 
        pass
    return [t.lower() for t in sentence]

class Heap(list):
    def init(self, *args):
        super(Heap, self).__init__(*args) 
    def push(self, element):
        if len(self) > config.beam:
            hq.heappushpop(self,element)
        else: 
            hq.heappush(self, element)
    def pop(self, element):
        return hq.heappop(self)
    def extend(self, elements):
        for e in elements: 
            hq.heappush(self, e)
    def nlargest(self, n, key = None):
        return hq.nlargest(n, self, key) if key else hq.nlargest(n, self)
    def nsmallest(self, n, key = None):
        return hq.nsmallest(n, self, key) if key else hq.nsmallest(n, self)


class ChartEntry(dict): 
    """
    Keep a list of chart items organized by the binarization level of their rule.
    Every key corresponds to a binarization level. Each value is a heap containing
    the chart items.
    """
    def init(self, *args): 
        super(dict, self).__init__(*args)
    def push(self,item):
        bin_level = item[1].rule.bin_level
        if not bin_level in self:
            self[bin_level] = [item] 
        else:   
            if len(self[bin_level]) == config.beam:
                hq.heappushpop(self[bin_level],item)
            else: 
                hq.heappush(self[bin_level],item)
    #def nlargest(self, n): 
    #    new = ChartEntry()
    #    for bin_level in self: 
    #        new[bin_level] = Heap(self[bin_level]).nlargest(n)
    #    return new
    #def nsmallest(self, n):
    #    new = ChartEntry()
    #    for bin_level in self: 
    #        new[bin_level] = Heap(self[bin_level]).nsmallest(n)
    #    return new

class Item():
    """
    A parse item that contains a list of it's components
    """
    def __init__(self, rule, lchild, rchild, i,j):
                
        self.rule = rule
        self.weight = math.log(rule.weight)
        self.symbol = self.rule.lhs
        self.i =i 
        self.j =j

        self.lchild = lchild
        self.rchild = rchild
        self.logprob = self.weight + (lchild.logprob if lchild else 0.0) + (rchild.logprob if rchild else 0.0) 

    def __repr__(self):
        return "[{0}:{1} {2}]".format(self.i, self.j, str(self.rule))

class CkyParser(object):

    def __init__(self, grammar):
        self.grammar = grammar

    def parse_sentence(self, sent, beam = config.beam):
        sentence = preprocess_sent(sent)
        rules_for_span = defaultdict(ChartEntry) # span -> Heap((max_score, item)) 
        n = len(sentence)
        for i in range(n): # first add all unary rules
            for rule in self.grammar.filter_rules(sentence[i]):                
                item = Item(rule, [], [], i, i+1)                
                rules_for_span[i,i+1].push((item.logprob, item))
            for unary_rule in self.grammar.filter_rules(rule.lhs):
                unary_item = Item(unary_rule, item, None, i, i+1)
                rules_for_span[i, i+1].push((unary_item.logprob, unary_item))

        for length in range(2,n+1):
            #print length
            for i in range(0,n-length+1): # all possible splits 
                for k in range(1,length):
                    try: 
                        lspan_items_bins = rules_for_span[i,i+k] #.nlargest(beam)
                        rspan_items_bins = rules_for_span[i+k,i+length] #.nlargest(beam)
                        lspan_items = lspan_items_bins[0] # Because of right binarization we don't have to include any incomplete left items
                        sum_right =0 
                        for bin_level in range(0,i+2):
                          if bin_level in rspan_items_bins:
                            rspan_items = rspan_items_bins[bin_level] 
                            sum_right += len(rspan_items)
                            for (lscore, litem), (rscore, ritem) in product(lspan_items, rspan_items): 
                                new_rule_sum = 0
                                for rule in self.grammar.filter_rules(litem.rule.lhs, ritem.rule.lhs): # Find all rules
                                    new_rule_sum += 1
                                    new_item = Item(rule, litem, ritem, i, i+length)
                                    rules_for_span[i, i+length].push((new_item.logprob, new_item))
                                    for unary_rule in self.grammar.filter_rules(rule.lhs):
                                        new_rule_sum += 1
                                        unary_item = Item(unary_rule, new_item, None, i, i+length)
                                        rules_for_span[i, i+length].push((unary_item.logprob, unary_item))
                        #print length, i, k, len(lspan_items), len(rspan_items), new_rule_sum 
                    except KeyError, e: 
                        continue
                                
                #print i, i+length-1, rules_for_span[i,i+length]
        #return sorted((r for r in rules_for_span[0,n] if r[1].rule.lhs == "S"),reverse = True)[:beam]
        return rules_for_span[0,n]

        #return sorted((r for r in rules_for_span[0,n]),reverse = True)[:beam]
                                
#def decode_chart(chart, n = 10):
#    subtrees_and_scores = []
#    for item in chart: 
#        left = decode_chart(item.lchildren,n)
#        right = decode_chart(item.rchildren,n)
#        if left and right: 
#            for treel, scorel in left:
#                for treer, scorer in right: 
#                    subtrees_and_scores.append((FancyTree(item.rule.lhs, [treel, treer]), item.weight + scorel + scorer))
#        else: 
#            subtrees_and_scores.append((FancyTree(item.rule.lhs, item.rule.rhs),item.weight))
#    return sorted(subtrees_and_scores, reverse=True, key = lambda x: x[1])[:n]

def decode_chart(item):
    subtrees_and_scores = []
    if not (item.lchild):
        return item.weight, FancyTree(item.rule.lhs, item.rule.rhs)
    else: 
        lscore, ltree = decode_chart(item.lchild)
        if item.rchild: 
            rscore, rtree = decode_chart(item.rchild)
            return lscore + rscore + item.weight, FancyTree(item.rule.lhs, [ltree, rtree])
        else: 
            return lscore + item.weight, FancyTree(item.rule.lhs, [ltree])
  
def tsg_derivation_from_chart_item(item):
    subtrees_and_scores = []
    if not (item.lchild):
        return item.weight, FancyTree(str(item.rule.original_tsg_rule), [])

    score = item.weight 
    children = []
    lscore, lchild = tsg_derivation_from_chart_item(item.lchild)
    score += lscore
    if "|" in item.lchild.rule.lhs: 
        children.extend(lchild)
    else: 
        children.append(lchild)

    if item.rchild: 
        rscore, rchild = tsg_derivation_from_chart_item(item.rchild)
        score += rscore
        if rchild is not None:
            if "|" in item.rchild.rule.lhs:
                children.extend(rchild)
            else:
                #if len(rchild) > 0:
                    children.append(rchild)
                #else: 
                #    children.extend(rchild.node)
    return score, FancyTree(str(item.rule.original_tsg_rule), tuple(children))

class EvaluationMode(object):
    macro = 0
    micro = 1

def evaluate_parser(parser,testset, tsg):
    """
    Run the parser on the gold POS sequence for each sentence in the testset, then compare the labeled 
    spans. 
    """    

    f1s = []

    for reference_tree in testset:
        reference_spans = set(reference_tree.get_constituents())
        
        gold_pos = reference_tree.leaves()
        if len(gold_pos) >= 40:
            continue
        chart = parser.parse_sentence(gold_pos)

        if chart and 0 in chart:            
            completed_binarizations = sorted(chart[0], reverse = True)
            # first_best = chart[0][1]
            #best_score, best_parse = decode_chart(first_best)
            #best_score, best_parse = decode_chart(completed_binarizations[0][1])
            #best_parse = best_parse.debinarize()
            score, derivation = tsg_derivation_from_chart_item(completed_binarizations[0][1])
            best_parse = tsg.apply_derivation(derivation)
            print reference_tree
            print best_parse
            predicted_spans = set(best_parse.get_constituents())

            local_correct = len(reference_spans & predicted_spans)

            local_recall = len(reference_spans & predicted_spans) / float(len(reference_spans))
            local_precision = len(reference_spans & predicted_spans) / float(len(predicted_spans))
            local_f1 = 2 * (local_precision * local_recall) / (local_precision + local_recall)
            print "Prec %f Rec %f F1 %f" % (local_precision, local_recall, local_f1)
            f1s.append(local_f1)

        else: 
            print "Could not parse %s" % " ".join(gold_pos)
            #f1s.append(0)
        print 
    
    accuracy = sum(f1s) / len(f1s)
    
    return accuracy            
            

    
class TestParser(unittest.TestCase):

    def setUp(self):
        grammar = StringGrammar() 
        grammar.add_rule(StringRule("S",("NP","VP"),1))

        grammar.add_rule( StringRule("NP",("DT","N"),0.4))
        grammar.add_rule( StringRule("NP",("i",),0.4))
        grammar.add_rule( StringRule("NP",("NP","PP"),0.2))

        grammar.add_rule( StringRule("VP",("V","NP"),0.5))
        grammar.add_rule( StringRule("VP",("VP","PP"),0.5))
        grammar.add_rule( StringRule("V",("saw",),1))

        grammar.add_rule( StringRule("PP",("P","NP"),1))
        grammar.add_rule( StringRule("P",("with",),1))

        grammar.add_rule( StringRule("DT",("the",),1))

        grammar.add_rule( StringRule("N",("telescope",),0.5))
        grammar.add_rule( StringRule("N",("hill",),0.5))
        self.grammar = grammar

    def test_parser(self):
        parser = CkyParser(self.grammar)
        chart = parser.parse_sentence("i saw the hill with the telescope")       
        score1, parse1 = decode_chart(chart[0][1]) 
        score2, parse2 = decode_chart(chart[1][1]) 
        print score1, parse1.pprint()
        print score2, parse2.pprint()
        self.assertAlmostEqual(math.e**score2, 0.0016)
        self.assertAlmostEqual(math.e**score1, 0.004)


def main():
    with open(sys.argv[1]) as grammar_file:        
        print "Loading grammar... "
        #grammar = StringGrammar.from_file(grammar_file)
        tsg = TreeSubstitutionGrammar.from_file(grammar_file)
        grammar = tsg.to_psg()
        parser = CkyParser(grammar)

        print "Loading treebank ... "
        treebank = Treebank.from_dirs(sys.argv[2:])

        print "Evaluating ..." 
        accuracy = evaluate_parser(parser, treebank, tsg)
        print "Accuracy: %f" % accuracy

        #inp = True
        #while inp: 
        #    string = raw_input()
        #    print "Parse"
        #    chart = parser.parse_sentence(string) 
        #    print "Done"
        #    try:
        #        if not chart: 
        #            raise KeyError
        #        for max_score, item in chart:
        #            score, parse = decode_chart(item)
        #            p2 = parse.debinarize()
        #            print score, p2
        #    except KeyError: 
        #        print "No parse."
 
    
if __name__ == "__main__":
    main()                       
    #with open(sys.argv[1]) as grammar_file:        
    #        print "Loading grammar... "
    #        tsg = TreeSubstitutionGrammar.from_file(grammar_file)
    #        grammar = tsg.to_psg()
    #        parser = CkyParser(grammar)
    #       
    #        #sent = "nnp nnp , nnp nnp vbz dt nn . ''" 

    #        sent= "nnp nnp , dt nn in jj jj nnp nnp wdt vbz nnp nns , vbd vbg nn in prp$ nn nn nns in cd ."    
    #        print "parsing..."
    #        chart = parser.parse_sentence(sent)
    #        completed_binarizations = sorted(chart[0], reverse = True)
    #        print completed_binarizations[0][1]
    #        score, deriv = tsg_derivation_from_chart_item(completed_binarizations[0][1])
    #        
    #        #score, parse = decode_chart(completed_binarizations[0][1])
    #        print score, deriv 

    #psg = tsg.to_psg()
    #parser = CkyParser(psg)
    #sentence = tree.leaves()
    #print " ".join(str(x) for x in enumerate(sentence))
    #chart = [(score, item) for score,item in parser.parse_sentence(sentence) if item.rule.lhs == "S"]
    #score, result =  decode_chart(chart[0][1]) 
    #print score, result
     
