from grammar.tree import Tree, ImmutableTree
from grammar.cfg import Nonterminal
from distribution import geometric

import os 
import re
import sys
import string
import copy
import random


nonterminal_table = {",":"-COMMA-",".":"-PERIOD-",":":"-COLON-","``":"-BGNQUOT-","''":"-ENDQUOT-"}

tracematcher = re.compile("([^-]+?)[-=].+$")

def match_trace(node):
    match = tracematcher.match(node)
    if not match: 
        return node
    return match.group(1)


class Constituent(object):
    def __init__(self, start, end, label):
        self.start = start
        self.end = end
        self.label = label
    
    def __repr__(self):
        return "(%s:[%i,%i])" % (self.label, self.start, self.end)

    def __hash__(self): 
        return  hash((self.start, self.end, self.label)) 

    def __eq__(self,other):
        return self.start == other.start and self.end == other.end and self.label == other.label


class FancyTree(ImmutableTree):

    def __init__(self, node_or_str, children=None, boundary_node=False):
        super(FancyTree,self).__init__(node_or_str, children)
        #self.size_cache = None
        self.is_boundary = False
        self.substitution_node = False
        self.boundary_nodes = {}
        self.treeprob = None

    def make_unary_rules(self):
        if len(self) > 1: 
            children =  [c.make_unary_rules() if isinstance(c, FancyTree) \
             else FancyTree(c.upper(), [c]) for c in self]
            return self.__class__(self.node, children)
        else: 
            c = self[0]
            new_child = c.make_unary_rules() if isinstance(c, FancyTree) else c 
            return self.__class__(self.node, [c])
        
    def binarize(self, childn = 2, branch = "right", decoration="", level=0):
        """
        return a new C{FancyTree} that is binarized.
        """
        parent = self.node
        if len(self) > 2:            
            lastnode = self[-1]
            new_subtree = lastnode.binarize(childn, branch,decoration, level) if isinstance(lastnode,FancyTree) else lastnode
            for i in range(len(self) - 2, 0, -1):
                new_label = "%s%s|%s#%i" % (parent, decoration, ";".join([x if type(x) is str else x.node for x in self[i:i+childn]]), i)
                if isinstance(self[i], FancyTree):
                    new_subtree = self.__class__(new_label, [self[i].binarize(childn, branch,decoration, level), new_subtree])
                else:
                    new_subtree = self.__class__(new_label, [self[i], new_subtree])
            if isinstance(self[0],FancyTree):
                if not level: 
                    return self.__class__("%s" % self.node, [self[0].binarize(childn, branch,decoration, level),new_subtree])
                else: 
                    return self.__class__("%s%s" % (self.node, decoration), [self[0].binarize(childn, branch,decoration, level),new_subtree])
            else: 
                if not level: 
                    return self.__class__("%s" % self.node, [self[0],new_subtree])
                else: 
                    return self.__class__("%s%s" % (self.node, decoration), [self[0],new_subtree])
        elif len(self) == 2:
            if not level: 
                return self.__class__("%s" % (self.node), [c.binarize(childn, branch, decoration, level) if isinstance(c, FancyTree) else c for c in self])
            else: 
                return self.__class__("%s%s" % (self.node, decoration), [c.binarize(childn, branch, decoration, level) if isinstance(c, FancyTree) else c for c in self])
        elif len(self) ==1: # Need to allow unary productions
                if isinstance(self[0], FancyTree):
                    if not level: 
                        return self.__class__("%s" % (self.node), [self[0].binarize(childn, branch, decoration, level)])
                    else: 
                        return self.__class__("%s%s" % (self.node,decoration), [self[0].binarize(childn, branch, decoration, level)])
                else:
                    return self.__class__("%s" % (self.node), [self[0]])
        else: 
            return self.__class__(self.node,[])

    def binarize_with_parent(self, childn = 2, branch = "right", prev = "root"):
        """
        return a new C{FancyTree} that is binarized.
        """
        parent = "%s^%s" % (self.node, prev)
        if len(self) > 2:
            new_subtree = self[-1].binarize(childn, branch)
            for i in range(len(self) - 2, 0, -1):
                new_label = "%s|%s" % (parent, ";".join([x.node for x in self[i:i+childn]]))
                new_subtree = self.__class__(new_label, [self[i].binarize_with_parent(childn, branch, self.node), new_subtree])
            return self.__class__(parent, [self[0].binarize_with_parent(childn, branch, self.node),new_subtree])
        else: 
            return self.__class__(parent, [c.binarize_with_parent(childn, branch, self.node) if isinstance(c, FancyTree) else c for c in self])

    def debinarize_to_dtree(self, childn = 2, branch = "right"):
        """
        Debinarize the tree but maintain TSG rule id annotations
        """
        def debinarize_inner(tree):
            rule_id = None
            if "|" in tree.node:
                parent, rest = tree.node.split("|")
                if "_" in parent:
                    nonterminal,rule_id = parent.split("_")
                    print "found ", nonterminal, rule_id
                collect = []
                res = [debinarize_inner(c) for c in tree]
                for sub_rule, r in res:
                    if type(r) is tuple: 
                        collect.extend(r)
                    else: 
                        collect.append(r)
                return rule_id,tuple(collect)
            else: 
                children = []
                for c in tree: 
                    if isinstance(c, FancyTree):
                        rule_id, res = debinarize_inner(c)
                        if type(res) is tuple:
                            children.extend(res)
                        else: 
                            children.append(res)
                    else: 
                        children.append(c)
                if "^" in tree.node: # has parent annotations
                    label = tree.node.split("^")[0]
                else:
                    label = tree.node
                if rule_id: 
                    label = "%s_%s" % (label, rule_id)
                #return rule_id, children
                return rule_id, self.__class__(label, children)


        return debinarize_inner(self)


    def debinarize(self, childn = 2, branch = "right"):
        """
        return a new C{FancyTree} that is not binarized.
        """

        def debinarize_inner(tree):
            if "|" in tree.node:
                collect = []
                res = [debinarize_inner(c) for c in tree]
                for r in res:
                    if type(r) is tuple: 
                        collect.extend(r)
                    else: 
                        collect.append(r)
                return tuple(collect)
            else: 
                children = []
                for c in tree: 
                    if isinstance(c, FancyTree):
                        res = debinarize_inner(c)
                        if type(res) is tuple:
                            children.extend(res)
                        else: 
                            children.append(res)
                    else: 
                        children.append(c)
                if "^" in tree.node: # has parent annotations
                    label = tree.node.split("^")[0]
                else:
                    label = tree.node
                return self.__class__(label, children)
        return debinarize_inner(self)


    def _get_const_inner(self): 
        yield_size = 0
        constituents = []
        for c in self: 
            if isinstance(c,Tree):
                consts, sub_yield_size = c._get_const_inner()
                for const in consts: 

                    const.start += yield_size
                    const.end += yield_size
                    constituents.append(const)                    
                yield_size += sub_yield_size
            else: 
                yield_size += 1  
        constituents.append(Constituent(0,yield_size,self.node))
        return constituents, yield_size

    def get_constituents(self):
        return self._get_const_inner()[0]

    def filter_subtrees(self, f, m = lambda x: x):
        #if f(self):
        gen = (c.filter_subtrees(f,m) if isinstance(c,Tree) else c for c in self if isinstance(c,str)  or f(c))
        res =  self.__class__(m(self.node),gen) 
        return res
        #else:
        #    return self.__class__(m(self.node), [])

    def preprocess(self):
        """
        remove traces, get rid of leaves, use only base-PP categories 
        (no PP-CLR, or PP-LOC...) 
        """
        children = []
        found_none = False
        preterminal = False
        for c in self: 
            try: 
                if c.node!="-NONE-":
                    d = c.preprocess()
                    if d is not None:
                        children.append(d)
                    else: 
                        found_none = True
                else: 
                    found_none = True
            
            except AttributeError:
                # leaf node
                preterminal = True
                children.append(c)

        if preterminal:
            assert(len(children) == 1)
            new_node = match_trace(self.node)
            if new_node in nonterminal_table:
                new_node = nonterminal_table[new_node]
            return self.__class__(new_node, [match_trace(self.node.lower())])

        # if we only found empty elements as children, this node should be pruned as well
        if found_none and not children: 
            return None

        if found_none and len(children) == 1: # Also remove any binary branching production that has a Null
            return children[0]

        #if self.node.startswith("PP-"):
        #    new_node = "PP"
        #else:     
        #    new_node = self.node
            
        return self.__class__(match_trace(self.node),children) 

    def get_tsg_fragment(self):
        return self.filter_subtrees(lambda x:not x.is_boundary)

    def get_fragment(self, boundary_nodes, prefix = tuple()):
        children = []
        new_boundary_nodes = copy.copy(boundary_nodes) 
        for position, c in enumerate(self): 
            try:
                new_prefix = prefix + (position,)

                if  new_prefix in boundary_nodes:  
                    new_boundary_nodes.add(new_prefix)
                    new_c = Nonterminal(c.node)
                    new_c.substitution_node = True
                    children.append(new_c)
                else:
                    sub_boundary_nodes, childtree = c.get_fragment(boundary_nodes, new_prefix)
                    new_boundary_nodes.update(sub_boundary_nodes)
                    children.append(childtree) 
            except AttributeError: 
                children.append(c)
        return new_boundary_nodes, self.__class__(self.node, children)

    def get_fragment_and_boundary_nodes(self):
        children = []
        boundary_nodes = []
        for c in self: 
            if isinstance(c, FancyTree):
                if  c.is_boundary: 
                    new_c = Nonterminal(c.node)
                    new_c.substitution_node = True
                    children.append(new_c)
                    boundary_nodes.append(c)
                else:
                    childtree, new_boundary_nodes = c.get_fragment_and_boundary_nodes()
                    boundary_nodes.extend(new_boundary_nodes)
                    children.append(childtree) 
            else: 
                children.append(c)
        return self.__class__(self.node, children), boundary_nodes

    def get_partitions(self):
        next_boundary_nodes = [self] 
        count = 0
        while next_boundary_nodes: 
            count += 1
            next_root = next_boundary_nodes.pop() 
            partition, new_next_boundary_nodes = next_root.get_fragment_and_boundary_nodes()
            next_boundary_nodes.extend(new_next_boundary_nodes)
            yield partition

    #def sample_boundary_nodes(self, outer = None):
    #    positions = self.treepositions()
    #    current_assignment = dict(self[position].is_boundary for position in positions)
        
 
    def size(self):
        #if self.size_cache is not None: # this tree is immutable, so caching is safe 
        #    return self.size_cache
        #else: 
            #self.size_cache = sum(c.size() if isinstance(c, Tree) else 1 for c in self)  + 1
            return sum(c.size() if isinstance(c, Tree) else 1 for c in self)  + 1
            #return self.size_cache

    def mark_random_boundary(self, root=True):
        if not root: 
            if not (len(self) == 1 and not isinstance(self[0], Tree)):
                self.is_boundary = random.choice([True, False])
        for c in self:
            if isinstance(c, Tree):
                c.mark_random_boundary(root=False)


    def mark_all_as_boundary(self, root=True):
        if not root: 
            self.is_boundary = True
        for c in self:
            if isinstance(c, Tree):
                c.mark_all_as_boundary(root=False)

    def pprint(self, margin=70, indent=0, nodesep="", parens='()', quotes=False):
        childstrs = []
        for child in self:
            if isinstance(child, Tree):
                childstrs.append(child.pprint(margin, indent,nodesep, parens, quotes))
            elif isinstance(child, tuple):
                childstrs.append("/".join(child))
            elif isinstance(child, basestring) and not quotes:
                childstrs.append('%s' % child)
            else:
                childstrs.append('%r' % child)
        if isinstance(self.node, basestring):
            return '%s%s%s%s %s%s' % (parens[0], self.node, nodesep, "@" if self.is_boundary or self.substitution_node else "",  
                                    string.join(childstrs), parens[1])
        else:
            return '%s%r%s%s %s%s' % (parens[0], self.node, nodesep,"@" if self.is_boundary or self.substitution_node else "",
                                    string.join(childstrs), parens[1])

    

def tree_iterator(dirlist):
    """
    provides a generator over C{FancyTree} objects from all data files in a directory.
    """
    for directory in dirlist: 
        for path, dirs, files in os.walk(directory):
                for f in sorted([x for x in files if x.endswith(".ptb")]): 
                    print("Reading trees from section %s" % f)
                    path_name = os.path.join(path, f)
                    with open(path_name,'r') as ptb_file: 
                        for l in ptb_file:
                            tree = FancyTree.parse(l.strip())
                            yield tree


class Treebank(list): 
    """
    Store the entire treebank! This is a huge object!
    """

    @classmethod 
    def from_dirs(cls, dirlist, binarize = False):
        result = cls()
        for t in tree_iterator(dirlist):
            tree = t[0].preprocess()
            #tree.mark_all_as_boundary()
            tree.mark_random_boundary()
            if binarize:
                tree =tree.binarize()
            result.append(tree)
            
        return result
        
        

if __name__ == "__main__":
    x = FancyTree("( (S (NP-SBJ (DT The) (NNP Lorillard) (NN spokeswoman) ) (VP (VBD said) (SBAR (-NONE- 0) (S (NP-SBJ-1 (NN asbestos) ) (VP (VBD was) (VP (VP (VBN used) (NP (-NONE- *-1) ) (PP-CLR (IN in) (`` ``) (NP (ADJP (RB very) (JJ modest) ) (NNS amounts) ) ('' '') ) (PP-LOC (IN in) (S-NOM (NP-SBJ (-NONE- *) ) (VP (VBG making) (NP (NP (NN paper) ) (PP (IN for) (NP (DT the) (NNS filters) )))))) (PP-TMP (IN in) (NP (DT the) (JJ early) (CD 1950s) ))) (CC and) (VP (VBN replaced) (NP (-NONE- *-1) ) (PP (IN with) (NP (NP (DT a) (JJ different) (NN type) ) (PP (IN of) (NP (NN filter) )))) (PP-TMP (IN in) (NP (CD 1956) )))))))) (. .) ))")

    #y = FancyTree("(S (IN in) (S|NP;VP (NP (NP (NNP nnp) (NNP nnp)) (NP|,;NP (, ,) (NP|NP;CC (NP (NP (NP (DT dt) (NP|NN;POS (NN nn) (POS pos))) (NP|JJS;NN (JJS jjs) (NP|NN;NN (NN nn) (NN nn)))) (PRN (, ,) (PRN|S;, (S (NP (NN nn) (NN nn)) (VP (VBD vbd) (ADVP (RB rb) (RB rb)))) (, ,)))) (NP|CC;NP (CC cc) (NP (PRP prp) (NP (VBD vbd) (QP (RB rb) (IN in)))))))) (S|VP; (VP (ADVP (RB rb) (ADVP|NP;PP (NP (PDT pdt) (NP|DT;NN (DT dt) (NN nn))) (PP (IN in) (NP (DT dt) (NN nn))))) (VP|VBG;NP (VBG vbg) (VP|NP;NP (NP (NNP nnp) (NNP nnp)) (NP (IN in) (CD cd))))) (S|;. .))))")


    #print x[0]
    #x = x[0].preprocess()
    #print
    #print x
    #x = FancyTree("(S (NP-SBJ (NP (NP (NNP nnp) (NP|NNP;, (NNP nnp) (, ,))) (NP|ADJP;, (ADJP (CD cd) (ADJP|NN;JJ (NN nn) (JJ jj))) (, ,))) (VBD vbd)) (NP (DT dt) (NP|NN;. (NN nn) (. .))))")

    #x = FancyTree("(S (NP-SBJ (NP (NP (NNP nnp) (NP|NNP;, (NNP nnp) (, ,))) (NP|ADJP;, (ADJP (CD cd) (ADJP|NN;JJ (NN nn) (JJ jj))) (, ,))) (VBZ vbz)) (NP (DT dt) (NP|NN;. (NN nn) (. .))))")
    #y =  x.debinarize()
    
   #
    #parts = x.partition()

    #for p in parts: 
    #    print " ".join(str(p).split())
    #    print 

    #treebank = Treebank.from_dirs(sys.argv[1:])
    
