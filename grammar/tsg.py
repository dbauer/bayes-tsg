import sys
import os
import math
import re
from grammar.fancy_tree import FancyTree, nonterminal_table
from grammar.cfg import Rule, CFG, Nonterminal
from grammar.psg import StringGrammar, StringRule, get_string_cfg_rules

import config
import copy
import random
import logging

bin_level_re =  re.compile(".*#(\d+)")


def is_nonterminal(label):
    return not any(c for c in label if c.isalpha  and c.islower())

class TsgRule(Rule):

    def __init__(self, lhs, rhs, rule_id = 0):
        super(TsgRule, self).__init__(lhs, rhs)
        self.rule_id = rule_id 

    def __repr__(self):
        tree_s = "Tree[%s]" %" ".join(str(self.rhs).split())
        return "<TSG Rule %s: %s>" % (str(self.rule_id), str(tree_s))
        
    def flatten_rhs(self):
        children = []

        if self.rhs.leaves() == list(self.rhs): 
            return self.rhs
        else: 
            children = self.rhs.leaves()
        #for l in self.rhs.leaves(): 
        #    if isinstance(l, FancyTree):
        #        children.append(Nonterminal(l.node))
        #    else: 
        #        if not is_nonterminal(l):
        #            if l in nonterminal_table:
        #                children.append(FancyTree(nonterminal_table[l],[l]))
        #            else: 
        #                children.append(FancyTree(l.upper(),[l]))
        #        else: 
        #            if l in nonterminal_table:
        #                children.append(FancyTree(nonterminal_table[l],[l]))
        #            else:  
        #                children.append(Nonterminal(l))
        return FancyTree(self.lhs, children) 

    def get_string_rules(self):
        rules =  get_string_cfg_rules(self.flatten_rhs().binarize(decoration="_%d" % self.rule_id))
        rules[0].weight = self.weight
        rules[0].original_tsg_rule = self.rule_id
        rules[0].bin_level = 0
        #weight = self.weight ** (1.0/len(rules))
        for rule in rules[1:]:
            if "#" in rule.lhs:
                forgetme, bin_level_s = rule.lhs.rsplit("#",1)
                match = bin_level_re.match(rule.lhs)
                if match: 
                    bin_level = int(bin_level_s)
                    rule.bin_level = bin_level
            rule.weight = 1.0 
            rule.original_tsg_rule = self.rule_id
        return rules
        
    def is_lexicalized(self):
        return any(not isinstance(x, Nonterminal) for x in self.rhs.leaves())
 

class TreeSubstitutionGrammar(CFG):

    def apply_derivation(self,tsg_derivation):
        this_etree = self.id_to_rules[int(tsg_derivation.node)].rhs
        if len(tsg_derivation) == 0:
            return this_etree
        
        subtrees = [self.apply_derivation(c) for c in tsg_derivation]
        subtrees.reverse()        

        def substitute_subtree(tree):
            children = []
            for c in tree: 
                if isinstance(c, FancyTree):
                    children.append(substitute_subtree(c)) 
                elif isinstance(c, Nonterminal):  # Substitution node
                    children.append(subtrees.pop())
                else:
                    if is_nonterminal(c): 
                        children.append(subtrees.pop())
                    else: 
                        children.append(c)
            return FancyTree(tree.node, tuple(children))
            
        return substitute_subtree(this_etree)
                        
    def to_psg(self): 
        psg = StringGrammar()
        for tsg_rule in self:
            for string_rule in tsg_rule.get_string_rules():
                psg.add_rule(string_rule)
        return psg
        
    def save(self,f):
        for c in self.find_lhs.keys():
            rules = []
            for rule in self.find_lhs[c]:
                if rule.count > 0:
                   mle = self.mle(rule)
                   rules.append((mle, rule))
            rules.sort()
            rules.reverse()
            for mle, rule in rules:  
                 f.write("{0}\t#{1}\n".format(str(rule.rhs),mle))
                  
    @classmethod 
    def from_file(cls, f):
        """
        Read in a tree substitution grammar from file
        """
        tsg = cls()
        id_counter = 0
        for line in f:
            rule_s, weight_s = line.strip().rsplit("#",1)
            etree = FancyTree(rule_s)
            rule = TsgRule(etree.node, etree)
            rule.weight = float(weight_s)
            rule.rule_id = id_counter
            id_counter += 1           
            if rule.weight >= 0.00001:
                tsg.add_rule(rule)
        return tsg
 
    def count_elementary_tree(self,etree):
        r =  TsgRule(etree.node, etree)
        self.count_rule(r)

    def extract_tsg_rules(self, ps_tree):
        """
        Get the tree substitution grammar rules according to the marked
        boundary nodes from the C{FancyTree} ps_tree, which is a phrase 
        structure tree. 
        """
        for etree in ps_tree.get_partitions():
            self.count_elementary_tree(etree)

    @classmethod
    def from_treebank(cls, treebank, string_grammar):       
        tsg = cls()
        tsg.string_grammar = string_grammar 
        for ps_tree in treebank: 
            tsg.extract_tsg_rules(ps_tree)
        return tsg

    def get_theta(self, etree):
        """
        get the subtree probability for a tree under the 
        chinese restaurant process.
        """
        string_grammar = self.string_grammar 
        r = TsgRule(etree.node, etree)
        count = self[r].count if r in self else 0
        try:
            total_with_root = self.lhs_count[etree.node] 
        except KeyError:
            total_with_root = 0
        base_measure = string_grammar.base_measure(etree)
        #logging.debug("base:"+str(math.e**base_measure))
        theta = (count + config.alpha * math.e**base_measure) / (total_with_root + config.alpha)
        #logging.debug("theta:"+ str(theta))
        return theta

    def sample_tree(self, tree):
        """
        sample a partitioning into fragments for a c{FancyTree} using the CRP,
        starting from a current boundary_node assignment. 
        """

        likelihood = 0

        new_boundary_nodes = set()
        for pos in tree.treepositions():
            try: 
                if tree[pos].is_boundary:
                    new_boundary_nodes.add(pos)
            except AttributeError:
                continue
                
        # This is difficult to do recursively, since changing the value of sibling nodes
        # means that the fragment under the previous boundary node changes. Instead 
        # we keep a set of boundary nodes and update it as we traverse the tree depth-first.
        #logging.debug(str(tree))
        stack = [(tree,tuple())]   # nodes still to sample
        
        boundary_node_stack = [tuple()] # parent boundary node of each node

        while stack:
      
            #logging.debug(str(new_boundary_nodes))
            #logging.debug(str([(x[1], x[0].node) for x in stack]))

            next_subtree, position = stack.pop()
            parent_boundary_node = boundary_node_stack.pop() 
            #logging.debug("-----------------------------------------")
            #logging.debug("Considering %s, %s, parent boundary node is %s" % (str(position), next_subtree, parent_boundary_node))
            if position: # don't resample root node 

                    was_boundary = False

                    # Get and score the joint fragment        
                    if position in new_boundary_nodes:
                        was_boundary = True
                        new_boundary_nodes.remove(position)
                    bnodes, join_etree = tree[parent_boundary_node].get_fragment(new_boundary_nodes, prefix = parent_boundary_node)
                    join_score = self.get_theta(join_etree)
            
                    #logging.debug("join fragment with score %f:\n   %s" % (join_score, str(join_etree)))
                    
                    # Then see what happens if we split the fragment            
                    new_boundary_nodes.add(position)
                    bnodes, parent_etree = tree[parent_boundary_node].get_fragment(new_boundary_nodes, prefix = parent_boundary_node)
                    parent_score = self.get_theta(parent_etree)
                    #logging.debug("parent fragment with score %f:\n   %s" % (parent_score, str(parent_etree)))
                    bnodes, child_etree = tree[position].get_fragment(new_boundary_nodes, prefix = position)
                    child_score = self.get_theta(child_etree)
                    #logging.debug("child fragment with score %f:\n   %s" % (child_score, str(child_etree)))
                    
                    # Now sample according to ratio
                    split_score = child_score * parent_score
                    #logging.debug("split score " +str(split_score))
                    ratio = join_score / (join_score + split_score) 
                    r = random.random()
                    is_boundary = (r > ratio)

                    join_rule = TsgRule(join_etree.node, join_etree) 
                    parent_rule = TsgRule(parent_etree.node, parent_etree) 
                    child_rule = TsgRule(child_etree.node, child_etree) 
                    if is_boundary: 
                        if was_boundary == False: 
                            self[join_rule].count -=1 
                            if self[join_rule].count == 0:
                                del self[join_rule]                          
                            try:
                                self[parent_rule].count +=1 
                                likelihood += math.log(self.mle(parent_rule)) 
                            except KeyError: 
                                self[parent_rule] = parent_rule
                                self[parent_rule].count = 1 
                            try:
                                self[child_rule].count +=1 
                                likelihood += math.log(self.mle(child_rule)) 
                            except KeyError: 
                                self[child_rule] = child_rule
                                self[child_rule].count =1 
                    else: 
                        if was_boundary:
                            try:
                                self[join_rule].count +=1 
                                likelihood += math.log(self.mle(join_rule)) 
                            except KeyError: 
                                self[join_rule] = join_rule
                                self[join_rule].count = 1 
                            self[parent_rule].count -=1 
                            if self[parent_rule].count == 0:
                                del self[parent_rule]                          
                            self[child_rule].count -=1 
                            if self[child_rule].count == 0:
                                del self[child_rule]                          
                        new_boundary_nodes.remove(position)
                        #logging.debug("sampled: join")
            next_local = [] 
            boundary_local = []
            for i, c in enumerate(next_subtree):
                if isinstance(c, FancyTree):
                    next_local.append((c, position+(i,))) 
                    if position and is_boundary: 
                        boundary_local.append(position)
                    else: 
                        boundary_local.append(parent_boundary_node)
            next_local.reverse()
            boundary_local.reverse()
            boundary_node_stack.extend(boundary_local)
            stack.extend(next_local)
    
        for p in tree.treepositions(): 
            t = tree[p]
            if p and isinstance(tree[p], FancyTree):
                t.is_boundary = (p in new_boundary_nodes)
        return likelihood 

def is_prefix(t1, t2):
    """
    True if tuple t1 is a prefix of tuple t2
    """
    if t1>t2: 
        return False
    for i,e1 in enumerate(t1):
        if t1[i] != e1: 
            return False
    return True
                    
def get_string_cfg_rules(tree):
    """
    recursively get a list of rules used in a tree.
    """
    rhs = []
    rules_from_subtrees = []
    for c in tree: 
        if isinstance(c, FancyTree):
            rhs.append(Nonterminal(c.node))
            rules_from_subtrees.extend(get_string_cfg_rules(c))
        else: 
            if is_nonterminal(c):
                rhs.append(Nonterminal(c))
            else:
                rhs.append(c.lower())
    r = StringRule(tree.node, tuple(rhs))
    return [r] + rules_from_subtrees 
    


if __name__ == "__main__":
    #g = StringGrammar.train_mle(sys.argv[1:])
    with open(sys.argv[1],'r') as grammar_file: 
        tsgrammar = TreeSubstitutionGrammar.from_file(grammar_file)
        psgrammar = tsgrammar.to_psg()
        #with open(sys.argv[2],'w') as out_file: 
        #    psgrammar.save(out_file)
