from collections import defaultdict

class Nonterminal(str):
    def __init__(self, s):
        super(Nonterminal, self).__init__(s)
        self.node = s


class Rule(object):
    def __init__(self, lhs, rhs, weight = None):
        self.lhs = lhs
        self.rhs = rhs
        self.weight =  weight
        self.count = 0
        self.rule_id = None
        self.bin_level = 0 

    def __eq__(self,other): 
        return self.lhs == other.lhs and self.rhs == other.rhs
        
    def __hash__(self):    
        return hash(self.lhs) * 17 + hash(self.rhs) * 3
    

class CFG(dict):

    def __init__(self):
        self.lhs_count =  {}
        self.find_lhs = {} 
        self.rhs_to_rules = {} 

        self.id_to_rules = {}
    
    def add_rule(self,r): 
        self[r] = r
        if not r.lhs in self.find_lhs: 
            self.find_lhs[r.lhs] = set()
        self.find_lhs[r.lhs].add(r)
        if not r.rhs in self.rhs_to_rules:
            self.rhs_to_rules[r.rhs] = set()
        self.rhs_to_rules[r.rhs].add(r)
        self.id_to_rules[r.rule_id] = r

    def get_rule_prob(self, key):
        raise NotImplementedException

    def count_rule(self, r):
        if r in self: 
            self[r].count += 1
        else: 
            self[r] = r
            self[r].count = 1
        if r.lhs in self.lhs_count: 
            self.lhs_count[r.lhs] += 1
        else: self.lhs_count[r.lhs] = 1  
        
        if not r.lhs in self.find_lhs: 
            self.find_lhs[r.lhs] = set()    
        self.find_lhs[r.lhs].add(self[r])

    def mle(self, r):
        return self[r].count / float(self.lhs_count[r.lhs])

    def __repr__(self):
        return str(self.keys())


    def size(self):
        """
        Also performs some memory clean up, deleting rules with 0 probability
        """
        count = 0
        for x in self: 
            if x.count > 0: 
                count += 1
            else: 
                del self[x] 
        return count

    def filter_rules(self, *symbols):
        try: 
            return self.rhs_to_rules[symbols]
        except KeyError:
            return [] 
