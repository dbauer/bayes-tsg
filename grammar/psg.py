import sys
import os
import math
from grammar.fancy_tree import FancyTree, tree_iterator, Treebank
from grammar.cfg import Rule, CFG, Nonterminal
import config
import distribution

from util import memoize

class StringRule(Rule):

    def __init__(self, lhs, rhs, weight=1.0):
        super(StringRule, self).__init__(lhs, rhs, weight)
        self.original_tsg_rule = None

    def is_lexicalized(self):
        return any(not isinstance(x, Nonterminal) for x in self.rhs)

    def __repr__(self):
        return "<String CFG Rule: %s -> %s>" % (self.lhs, " ".join(self.rhs)) 
        
    
class StringGrammar(CFG):

    def __init__(self):
        super(StringGrammar, self).__init__()

    alpha = config.pcfg_alpha
    
    @classmethod
    def from_file(cls, f):
        g = StringGrammar()
        for line in f:
            l = line.strip()
            try: 
                r, w = l.rsplit("#",1)
            except ValueError:
                print l
            weight = float(w)
            try:
                lhs, rhs = r.split("->")
            except: 
                 print r
            lhs = lhs.strip()
            rhs = tuple(rhs.strip().split(" "))
            rule = StringRule(lhs, rhs, weight=weight)
            g.add_rule(rule)
        return g

    def get_rule_prob(self, key):
        if key in self: 
            if not self[key].weight: 
                self[key].weight = self.compute_weight(self[key])
            return self[key].weight
        else: 
            try: 
                return math.log(StringGrammar.alpha) - \
                       math.log(self.lhs_count[key.lhs] + StringGrammar.alpha*(len(self.find_lhs[key.lhs])+1))
            except KeyError:
                sys.stderr.write("Warning: unknown LHS symbol %s \n" %key.lhs)

                return 0.0  
        
    def compute_weight(self, rule):

        try:
            return  math.log(float(rule.count+StringGrammar.alpha)) - \
                    math.log(self.lhs_count[rule.lhs] + StringGrammar.alpha*(len(self.find_lhs[rule.lhs])+1))
        except KeyError:
            return 0.0

    def count_tree(self, tree):
        for r in get_string_cfg_rules(tree):
            self.count_rule(r)

    @classmethod
    def train_mle(cls,treebank):
        grammar = StringGrammar()
        for tree in treebank:
            grammar.count_tree(tree)
        return grammar
 
    def save(self,f):
        for c in self.find_lhs.keys():
            rules = []
            for rule in self.find_lhs[c]:
                if rule.count > 0:
                   mle = self.mle(rule)
                   rules.append((mle, rule))
            rules.sort()
            rules.reverse()
            for mle, rule in rules:  
                 f.write("{0} -> {1}\t#{2}\n".format(rule.lhs, " ".join(rule.rhs),mle))
    

    def tree_prob(self, tree): 
         if tree.treeprob is not None: 
            return tree.treeprob
         else: 
            treeprob = sum(self.get_rule_prob(r) for r in get_string_cfg_rules(tree))
            tree.treeprob = treeprob
            return treeprob

    def base_measure(self, tree):
        """
        Returns the likelihood geometric(size(tree)) * tree_prob
        """
        x =  self.tree_prob(tree)
        y =  math.log(distribution.geometric(config.p, tree.size()))
        return x + y 
              
def get_string_cfg_rules(tree):
    """
    recursively get a list of rules used in a C{FancyTree} representing
     a phrase structure tree.
    """
    rhs = []
    rules_from_subtrees = []
    for c in tree: 
        if isinstance(c, Nonterminal): 
            rhs.append(c)
        else: 
            try:
                rhs.append(Nonterminal(c.node))
                rules_from_subtrees.extend(get_string_cfg_rules(c))
            except AttributeError:  
                rhs.append(c.lower())
    r = StringRule(tree.node, tuple(rhs))
    return [r] + rules_from_subtrees 
    
if __name__ == "__main__":
    #g = StringGrammar.from_file(open("out.psg","r"))
    g = StringGrammar.train_mle(Treebank.from_dirs(sys.argv[1:], binarize = True))
    #tree = FancyTree.parse("(S (NP-SBJ (NNP Dr.) (NNP Talcott) ) (VP (VBD led) (NP (NP (DT a) (NN team) ) (PP (IN of) (NP (NP (NNS researchers) ) (PP-DIR (IN from) (NP (NP (DT the) (NNP National) (NNP Cancer) (NNP Institute) ) (CC and) (NP (NP (DT the) (JJ medical) (NNS schools) ) (PP (IN of) (NP (NP (NNP Harvard) (NNP University) ) (CC and) (NP (NNP Boston) (NNP University) )))))))))) (. .) )")
    with open("out.psg","w") as out_file: 
        g.save(out_file)
