from grammar import fancy_tree, psg, tsg
import config
import sys
import cProfile
import gc 

import logging
logging.basicConfig(level=logging.WARN)

logfile = open("train_alpha100_i100.log",'w')


def print_best_n(tree_grammar,n):
    rules = sorted([(tree_grammar.mle(r), r) for r in tree_grammar.keys()])
    rules.reverse()
    for r in rules[:n]: 
        print r

def print_best_n(tree_grammar,n, category):
    rules = sorted([(tree_grammar.mle(r), r) for r in tree_grammar.find_lhs[category]])
    rules.reverse()
    psum = 0.0
    for s,r in rules[:n]: 
        #print s,r
        psum += s
    logfile.write(str(psum))
    print psum
    logfile.write("\n")


def print_perc(perc):
    sys.stdout.write(4*'\b')
    sys.stdout.write(str(int(perc*100)))
    sys.stdout.write("%")
    sys.stdout.flush()



print "Reading treebank"
treebank = fancy_tree.Treebank.from_dirs([sys.argv[1]])
print "Initializing string cfg"
string_grammar = psg.StringGrammar.train_mle(treebank) 

print "Initializing tree substitution grammar"
tree_grammar = tsg.TreeSubstitutionGrammar.from_treebank(treebank, string_grammar)


# initialize all rules
# Option 1: Just keep full trees 
# Option 2: Depth 1 tree fragments = CFG (i.e all nodes are boundary nodes) 
# Option 1 and 2 will lead to splitting or joining to be very unlikely if the initial TSG is extracted from the same data.
# Option 3: random (P(split) = P(join) = 0.5 

logfile.write("SIZE: " + str(tree_grammar.size())+ "\n")
print("SIZE: " , str(tree_grammar.size()))
#print_best_n(tree_grammar, 100, "S")
#print_best_n(tree_grammar, 100, "NP-SBJ")
#print_best_n(tree_grammar, 100, "NP")
#print_best_n(tree_grammar, 100, "VP")
#print_best_n(tree_grammar, 100, "PP")


total_trees = len(treebank)
print "Total trees in treebank: ",total_trees
logfile.write("Total trees in treebank: " + str(total_trees))


def iteration():
    count = 0
    total_ll = 0
    for t in treebank: 
        print_perc(count/float(total_trees))
        likelihood = tree_grammar.sample_tree(t)
        #if count % 1000 == 0:
        #    sys.stdout.write(".")
        #    sys.stdout.flush()
        #   print len(tree_grammar)
        #    gc.collect()    
        #objgraph.show_most_common_types()
        count = count + 1 
        total_ll += likelihood 
    sys.stdout.write("\n")
    sys.stdout.write("approx. LL under new grammar:%s\n " % str( total_ll))
    logfile.write("approx. LL under new grammar:" + str( total_ll))

for i in range(1, config.iterations+1):   
    print "Starting iteration %d" % (i)
    logfile.write("Starting iteration %d\n" % (i+1))
    iteration()
    print "SIZE: ", tree_grammar.size()
    logfile.write("SIZE: " + str(tree_grammar.size())+ "\n")
    #print_best_n(tree_grammar, 100, "S")
    #print_best_n(tree_grammar, 100, "NP-SUBJ")
    #print_best_n(tree_grammar, 100, "NP")
    #print_best_n(tree_grammar, 100, "VP")
    #print_best_n(tree_grammar, 100, "PP")
    logfile.write("\n")
    if i % 10 == 0:
        with open("iteration%s.tsg" % str(i),'w') as f:
            tree_grammar.save(f)
    print
with open("iteration%s.tsg" % str(i),'w') as f:
    tree_grammar.save(f)
