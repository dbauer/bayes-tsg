from parser.cky_parser import CkyParser, decode_chart
from grammar.tsg import TreeSubstitutionGrammar
from grammar.psg import StringGrammar
import sys

if __name__ == "__main__":
    with open(sys.argv[1]) as grammar_file:        
        print "Loading grammar... "
        tsg = TreeSubstitutionGrammar.from_file(grammar_file)
        print "Converting to psg..."
        psg = tsg.to_psg()

        parser = CkyParser(psg)
        
        inp = "dt nn vbz dt nn ." 
        chart = parser.parse_sentence(inp)

        print chart

        score, parse = decode_chart(chart[0][1]) 
        print score, parse

