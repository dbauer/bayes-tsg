def geometric(p, k):
    return (1-float(p)) ** (k-1) * p
